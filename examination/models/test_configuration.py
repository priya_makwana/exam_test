from openerp import models,fields,api, _
from openerp.exceptions import Warning
import logging
import random
from openerp.osv import osv

class student_exam_questions(models.Model):
    _name = 'test.configuration'
    name = fields.Char('Name',size=128)
    active = fields.Boolean('Active', defaults = True)
    subject_ids = fields.One2many('test.subject.configure', 'configuration_id', 'Subjects')
    domain_id = fields.Many2one('exam.domain', 'Domain')
    negative_marks_rate = fields.Float("Negative Marks Rate")
    
    
class test_subject_configure(models.Model):
    _name = 'test.subject.configure'
    _rec_name = 'category_id'
#     page_id = fields.Many2one('survey.page', 'Subject')
    category_id = fields.Many2one('survey.category', 'Category')
    number = fields.Integer(compute = '_compute_total_question',string ='Total Questions')
    categ_ids = fields.One2many('question.categ.configure', 'subject_id', 'Category')
    configuration_id = fields.Many2one('test.configuration', 'Test')
                

    @api.multi
    def _compute_total_question(self):
        for obj in self:
            total_question = 0
            for categ_id in obj.categ_ids:
                total_question = categ_id.ratio + total_question
            obj.number = total_question
            
            
class question_categ_configure(models.Model):
    _name = 'question.categ.configure'
    
    difficulty = fields.Selection([
                            ('very_easy','Very Easy'),
                            ('easy','Easy'),
                            ('hard','Hard'),
                            ('extreme_hard','Extreme Hard')
                            ], 'Difficulty')
    ratio = fields.Integer('Question Numbers')
    subject_id = fields.Many2one('test.subject.configure', "Subject")
            

class college_test_configuration(models.Model):
    _name="college.test.configuration"
    name = fields.Char("name")
#     question_ids = fields.Many2many('survey.question','college_question_relation','question_id','college_config_id',string = "Questions")
    page_ids = fields.Many2many('survey.page','college_page_relation','page_id','college_config_id',string = "Pages")
    domain_id = fields.Many2one('exam.domain', 'Domain',required = True)
    college_id = fields.Many2one('college.master',"College")
    
    
    
    @api.multi
    def get_random_questions(self):
#         test_domain_id = self.domain_id and self.domain_id.id
        test_record = self.env['test.configuration'].search([('domain_id','=',self.domain_id and self.domain_id.id)])
#             subject_ids = test_record.subject_ids
        if test_record:
            survey_page_ids =[]
            for subject in test_record[0].subject_ids:
                category_id = subject.category_id and subject.category_id.id
                question_categories_ids = subject.categ_ids
                for category in question_categories_ids:   
#                     difficulty = category.difficulty
                    page_id_list = []
#                     total_question = category.ratio
                    page_ids = self.env['survey.page'].search([('survey_category_ids','=',category_id)])
                    for page_id in page_ids:
                        for question_id in page_id.question_ids:
                            if question_id.difficulty == category.difficulty:
                                page_id_list.append(page_id.id)
#                     page_id_list = random.sample(page_id_list,category.ratio)
                    max_question = category.ratio
                    if len(page_id_list) < max_question :
                        max_question = len(page_id_list)
                    survey_page_ids += random.sample(page_id_list,max_question)
#                     for question in question_list:
#                         page_id_list.append(question.id)
                        
            self.write({'page_ids':[(6, 0,survey_page_ids)]})           
        return True      
                 
                 
    @api.multi
    def set_random_questions_for_all_students(self):
        test_domain_id = self.domain_id and self.domain_id.id
        college_id = self.college_id and self.college_id.id
        if test_domain_id and college_id:
            test_record = self.env['college.test.configuration'].search([('college_id','=',college_id),('domain_id','=',test_domain_id)])
            page_ids = test_record.page_ids
            student_records = self.env['student.master'].search([('college_id','=',college_id)])   
            if student_records and page_ids:
                for student in student_records:
                    test_configure_ids = []
                    random_page_ids = random.sample(page_ids,len(page_ids))
                    count = 1
                    student_exam_domain_configure_list = [] 
                    for domain_configure_id in student.question_domain_configure_ids:
                        student_exam_domain_configure_list.append(domain_configure_id.domain_id.id)
                    if test_domain_id not in student_exam_domain_configure_list:
                        for record in random_page_ids:
                            test_configure_page_id = self.env['student.test.configure'].create({'page_ids':record.id,'sequence':count,'student_id':student.id})
                            count = count + 1
                            test_configure_ids.append(test_configure_page_id.id)
                        self.env['student.exam.domain.configure'].create({'student_id':student.id,'domain_id':test_domain_id,'student_test_configure_ids':[(6,0,test_configure_ids)]})
#                         student_id.write({'question_ids':[(0, 0,{'question':record.question,'page_id':record.page_id.id,'difficulty':record.difficulty})]})
                    
    
class college_master(models.Model):
    _name="college.master"
    name  = fields.Char("Name",size=128)
    code  = fields.Integer("Code")
    active = fields.Boolean("Active", defaults= True)
    from_dt = fields.Date("From date",required = True)
    to_dt = fields.Date("To Date")
    test_configure = fields.One2many('college.test.configuration','college_id','Configure Test')


class student_test_configuration(models.Model):
    _name= "student.test.configure"
    name = fields.Char("name")
#     question_ids = fields.Many2one('survey.question',string = "Questions")
    page_ids = fields.Many2one('survey.page',string = "Page")
    sequence = fields.Integer("Question Sequence")
    student_id = fields.Many2one('student.master',"Student")
    student_exam_domain_configure_id = fields.Many2one('student.exam.domain.configure',"Student Test Domain Configure")
    
    
class student_master(models.Model):

    _name="student.master"
    
    name  = fields.Char("Name",size=64)
    code  = fields.Char("Student Code",size=64)
    email = fields.Char("Email",size=64)
    street =  fields.Char('Street', size=128)
    street2 = fields.Char('Street2', size=128)
    contact_no  = fields.Char("Contact Number",size=128)
    city = fields.Char("City",size=64)
    college_id  = fields.Many2one("college.master","College")
    survey_input_id = fields.Many2one('survey.user_input',"Survey Response",compute = 'get_survey_response_id')
    total_question = fields.Integer('Total Questions')
    total_attempt = fields.Integer('Total Attempted Questions')
    correct_answer = fields.Integer('Correct Answers')
    marks = fields.Float('Marks Obtain')
    enrolment_no = fields.Char('Enrolment Number', size=32)
    enrolment_no_confirm = fields.Boolean('Enrolment Number')
    stream_id = fields.Many2one('stream.master','Stream')
    percentage = fields.Float('Percentage', digits=(16,2))
    platform  = fields.Selection([
                                     ('erp','ERP'),
                                     ('ecommerce','E-Commerce'),
                                     ('both','Both'),
                                     ],"Domain")
                
    roll_no  = fields.Integer("Roll Number")
    gender = fields.Selection([
                                     ('male','Male'),
                                     ('female','Female'),
                                     ],"Gender")
 
#                student_department = fields.many2one('student.department','Department'),
#                student_course = fields.many2one('student.course','Course'),
                
    percentage_10 = fields.Float("10th Percentage",digits=(4,2))
    percentage_12 = fields.Float("12th Percentage",digits=(4,2))
    graduation_course = fields.Selection([
                                     ('BCA','BCA'),
                                     ('BCOM','BCOM'),
                                     ('B.Sc.','B.Sc.'),
                                     ('BBA','BBA'),
                                     ('OTHER','OTHER'),
                                     ],"Graduation")               
    graduation_percentage  = fields.Float("Graduation Percentage",digits=(4,2))
    graduation_inst = fields.Char('Graduation Institute', size=32)
                
    current_percentage = fields.Float("Current Percentage",digits=(4,2))
    domain_id = fields.Many2one('exam.domain', 'Domain')
    question_ids = fields.One2many('survey.question','student_id',string="Questions")
    question_sequence = fields.Integer("Quenstion Sequence")
    selected_for_interview = fields.Boolean("Is Selected for Interview?")
    question_domain_configure_ids = fields.One2many('student.exam.domain.configure','student_id','Configure Domain Questions')
    
    
    _sql_constraints = [
    ('enrolment_no_unique', 'unique(enrolment_no)', 'Enrolment Number already exists!')
    ]
    
    @api.multi
    def get_survey_response_id(self):
        for record in self:
            survey_user_response_id = self.env['survey.user_input'].search([('token','=',record.enrolment_no)])
            record.survey_input_id = survey_user_response_id and survey_user_response_id.id
    
    
    @api.multi
    def set_domain(self,domain_id):
        if not self.domain_id:
            self._cr.execute(""" update student_master set domain_id = %s where id = %s""" %(domain_id, self.id))
            self._cr.commit()
        elif self.domain_id and self.domain_id.id != int(domain_id):
            return "not properly set"
        return True
        
        
    @api.multi
    def set_random_questions(self):
#         test_domain = self.domain_id and self.domain_id.id
#         college_id = self.college_id and self.college_id.id

        if self.college_id:
            test_ids = self.env['student.exam.domain.configure'].sudo().search([('student_id','=',self.id),('domain_id','=',self.domain_id.id)])
            
            college_test_records = self.env['college.test.configuration'].search([('college_id','=',self.college_id.id)])
            student_exam_domain_configure_list = [] 
            for record in self.question_domain_configure_ids:
                student_exam_domain_configure_list.append(record.domain_id)
            for test_record in college_test_records:
                if test_record.domain_id not in student_exam_domain_configure_list:
                    test_configure_ids = []
                    page_ids = test_record.page_ids
                    random_questions = random.sample(page_ids,len(page_ids))
                    count = 1
                    for record in random_questions:
    #                 self.write({'question_ids':[(0, 0,{'question':record.question,'page_id':record.page_id.id,'difficulty':record.difficulty})]})
                        test_configure_page_id = self.env['student.test.configure'].create({'page_ids':record.id,'sequence':count,'student_id':self.id})
                        count = count + 1
                        test_configure_ids.append(test_configure_page_id.id)
                    
                    self.env['student.exam.domain.configure'].create({'student_id':self.id,'domain_id':test_record.domain_id.id,'student_test_configure_ids':[(6,0,test_configure_ids)]})
        return True
                
class survey_category(models.Model):
    _name = 'survey.category'
    
    
    @api.multi
    def get_total_page(self):
        res = {}  
        for record in self:
            record.total_page = int(self.env['survey.page'].search_count([('survey_category_ids','=',record.id)])) or 0
            
     
    name = fields.Char("Name",size = 128)
    color = fields.Integer('Color index')
    test_type = fields.Boolean('Is it test type?')
    members  = fields.Many2many('res.users', 'question_category_user_rel', 'question_category_id', 'uid', 'Members')
    required = fields.Selection([
                                              ('daily','Daily'),
                                              ('weekly','Weekly'),
                                              ('bi_weekly','Bi weekly'),
                                              ('monthly','Monthly'),
                                              ], 'Required')
    limit = fields.Integer('Limit')
    total_page = fields.Integer(String = "Total Page",compute="get_total_page")
    next_reminder_date = fields.Date('Next Reminder')
    description = fields.Text("Discription")
    survey_id = fields.Many2one('survey.survey',"Survey")
    page_ids = fields.One2many('survey.page','survey_category_ids',"Page")
    survey_category_weight = fields.Float("Weightage (%)")
        
class stream_master(models.Model):
    _name = 'stream.master'
    
    name = fields.Char('Stream', size=128)
                
    
class exam_domain(models.Model):
    _name = 'exam.domain'
    
    name   = fields.Char('Exam Domain', size=32)
    active = fields.Boolean("Active")

class student_exam_domain(models.Model):
    _name = 'student.exam.domain.configure'
    name =  fields.Char("name")
    domain_id = fields.Many2one('exam.domain',"Domain")
    student_id = fields.Many2one('student.master',"Student")
    student_test_configure_ids = fields.One2many('student.test.configure','student_exam_domain_configure_id',"Student Exam Question Configure")

