import openerp
from openerp.osv import osv
from openerp import models,fields,api, _
from openerp.exceptions import Warning
from datetime import datetime, timedelta,date
from pytz import timezone
import time

import logging

_logger = logging.getLogger(__name__)


class survey_survey(models.Model):
    _inherit = 'survey.survey'
    emipro_exam = fields.Boolean("Is Emipro Exam Survey?")

class interview_test(models.Model):
    _inherit = 'survey.question'
    # inherit survey.question model and add custom field
    question =  fields.Html('Question', size=2000, required=1)
    answer = fields.Char('Answer', size=2000)
    answer_explanation = fields.Html('Answer Explanation')
    answer_sequence = fields.Integer('Answer Sequence')
    question_detail = fields.Html('Question Detail')
    create_uid = fields.Many2one('res.users','Created By', size=256)
    create_date = fields.Datetime('Create Date')
    student_id = fields.Many2one('student.master','Student')
    difficulty = fields.Selection([
                                    ('very_easy','Very Easy'),
                                    ('easy','Easy'),
                                    ('hard','Hard'),
                                    ('extreme_hard','Extreme Hard'),
                                        ], 'Difficulty')
    state = fields.Selection([
                                    ('draft', 'Draft'),
                                    ('approved', 'Approved'),
                                    ],'State')
                #EPTchanges_04/06/2016
    domain_ids = fields.Many2many('exam.domain', 'survey_domain_rel', 'survey_id', 'domain_id', 'Domain')
    category_id = fields.Many2one('survey.category',"Category")
    _defaults = {
                 'state' : 'draft',
                 }
    
    
    @api.onchange('question')
    def copy_question(self):  
        '''
            Called for copy question in Question details
        '''
        if self.question:
            self.question_detail = self.question


    @api.multi
    def action_edit_question(self):
        """
            called for design question in html layout and this method is called by button and redirect to html layout
        """
        if not len(self) == 1:
            raise ValueError('One and only one ID allowed for this action')
        context = self._context or {}
        if not context.get('params'):
            action_id = self.env['ir.model.data'].xmlid_to_res_id('base.action_res_users')
        else:
            action_id = context['params']['action']
        url = '/design_question?model=survey.question&res_id=%s&return_action=%d' %(self.id,action_id)
        return {
            'name': _('Open with Visual Editor'),
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'self',
        } 

class survey_page(models.Model):
    _inherit = 'survey.page'
      
    @api.multi
    def get_total_question(self):
        res = {}  
        for record in self:
            self._cr.execute("""select count(id) from  survey_question where page_id = %s""" %(record.id))
            val = self._cr.dictfetchone()
            record.total_question = val['count'] or 0
       
       
    color = fields.Integer('Color index')
    test_type = fields.Boolean('Is it test type page ?')
    members  = fields.Many2many('res.users', 'page_user_rel', 'page_id', 'uid', 'Members')
    required = fields.Selection([
                                              ('daily','Daily'),
                                              ('weekly','Weekly'),
                                              ('bi_weekly','Bi weekly'),
                                              ('monthly','Monthly'),
                                              ], 'Required')
    limit = fields.Integer('Limit')
    total_question = fields.Integer(String = "total_questions",compute="get_total_question")
    next_reminder_date = fields.Date('Next Reminder')
    survey_category_ids = fields.Many2one('survey.category',"Category")
            
         
class survey_user_input(models.Model):
    _inherit = 'survey.user_input'
    
    
    @api.multi
    def calculate_result(self):
        for user_input in self:
            exam_marks = sum([uil.quizz_mark for uil in user_input.user_input_line_ids] or [0.0])
            student_id = self.env['student.master'].sudo().search([('enrolment_no','=',user_input.token)],limit = 1)
            if student_id:
                test_configuration_id = self.env['test.configuration'].sudo().search([('domain_id','=',student_id.domain_id.id)])
                student_exam_domain_configure_id = self.env['student.exam.domain.configure'].sudo().search([('student_id','=',student_id.id),('domain_id','=',student_id.domain_id.id)])
                if student_exam_domain_configure_id:
                    total_questions = int(len(student_exam_domain_configure_id.student_test_configure_ids))
                    total_attempted_questions = int(self.env['survey.user_input_line'].sudo().search_count([('user_input_id','=',self.id),('skipped','=',False)]))
                    total_correct_ans = int(self.env['survey.user_input_line'].sudo().search_count([('user_input_id','=',self.id),('quizz_mark','>',0)]))
                    exam_marks = exam_marks - ((total_attempted_questions - total_correct_ans) * test_configuration_id.negative_marks_rate)
                    percentage = exam_marks * 100 / total_questions
                    student_id.write({'marks':exam_marks,'percentage':percentage,'total_question':total_questions,'total_attempt':total_attempted_questions,
                                         'correct_answer':total_correct_ans,'enrolment_no_confirm':True})
                
            return exam_marks
