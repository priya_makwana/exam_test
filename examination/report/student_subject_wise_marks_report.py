from openerp import models, fields, api, _
import datetime
from cStringIO import StringIO
import base64
from openerp.osv import osv
# from pdftools.pdfdefs import false
from operator import itemgetter
try:
    import xlwt
    from xlwt import Borders
except ImportError:
    xlwt = None



class student_master(models.Model):
    _inherit ="student.master"
    datas= fields.Binary("Datas") 
    result_report_file_name = fields.Char("Report File Name")
    
    @api.multi
    def get_report(self):
        header_list = []
 
        workbook = xlwt.Workbook()
        borders = Borders()
        borders.left,borders.right,borders.top,borders.bottom = Borders.HAIR,Borders.HAIR,Borders.HAIR,Borders.HAIR
        right_border = Borders()
        right_border.right = Borders.HAIR
          
        header_bold = xlwt.easyxf("font: bold on; alignment: horizontal center;alignment: vertical center")
        header_bold_right = xlwt.easyxf("font: bold on; alignment: horizontal right")
        header_bold_left = xlwt.easyxf("font: bold on; alignment: horizontal left")
        
#         conditon_style = xlwt.easyxf("font: bold on; pattern: pattern solid, fore_colour gray25;")
 
        plain_style = xlwt.easyxf()
        plain_style.borders=right_border
         
        header_bold.borders=borders
        header_bold_left.borders = borders
        header_bold_right.borders = borders
        worksheet = workbook.add_sheet('Student Marks Report',cell_overwrite_ok=True)
        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        def get_width(num_characters):
            return int((1+num_characters) * 256)
         
        test_record = self.env['test.configuration'].search([('domain_id','=',self.domain_id.id)],limit = 1)
        date = datetime.datetime.today()
        subject_list = []
        for subject in test_record.subject_ids:
            subject_category_page_ids = []
             
            for page_id in subject.category_id.page_ids:
                subject_category_page_ids.append(page_id.id)
                 
            survey_user_input_id = self.env['survey.user_input'].search([('token','=',self.enrolment_no)])
            total_questions = subject.number
            correct_answers = self.env['survey.user_input_line'].search_count([('user_input_id','=',survey_user_input_id.id),('page_id','in',subject_category_page_ids),('quizz_mark','>',0)])
            total_attempt = self.env['survey.user_input_line'].search_count([('user_input_id','=',survey_user_input_id.id),('page_id','in',subject_category_page_ids),('skipped','=',False)])
             
            subject_list.append({'Subject':subject.category_id.name,'Total Questions':int(total_questions),'Total Attempts':int(total_attempt),'Correct Answers':int(correct_answers)})
        rows = 2
        worksheet.write(rows,0,"Name",header_bold)
        worksheet.write_merge(rows,rows,1,2,'%s' %(self.name))
        rows += 1
        worksheet.write(rows,0,"Enrollment No",header_bold)
        worksheet.write_merge(rows,rows,1,2,'%s' %(self.enrolment_no))
        rows += 1
        worksheet.write(rows,0,"Domain",header_bold)
        worksheet.write_merge(rows,rows,1,2,'%s' %(self.domain_id.name))
        rows += 1
        worksheet.write(rows,0,"College",header_bold)
        worksheet.write_merge(rows,rows,1,2,'%s' %(self.college_id.name))
        rows += 1
        worksheet.write(rows,0,"Date",header_bold)
        worksheet.write_merge(rows,rows,1,2,'%s'%(datetime.datetime.strftime(date, "%d-%m-%Y")))
        rows += 2
        header_list = ['Sr.No','Subject','Total Questions','Total Attempts','Correct Answers']
             
 
        for column,header in enumerate(header_list):
            worksheet.write(rows,column,header,header_bold)
             
        rows += 2   
        student_list = sorted(subject_list, key=itemgetter('Correct Answers'),reverse=True)
        for sr_no,student_record in enumerate(student_list):
            worksheet.write(rows,0,sr_no+1)
            worksheet.write(rows,1,student_record.get('Subject'))
            worksheet.write(rows,2,student_record.get('Total Questions'))
            worksheet.write(rows,3,student_record.get('Total Attempts'))
            worksheet.write(rows,4,student_record.get('Correct Answers'))
            rows += 1
                 
                 
                 
 
              
        fp = StringIO()
        workbook.save(fp)
        fp.seek(0)
        result_report_file = base64.encodestring(fp.read())
        fp.close()
        self.write({'datas':result_report_file,'result_report_file_name':'%s marks_report.xls' %(self.name)})
        return {
                'type' : 'ir.actions.act_url',
                'url':   '/web/binary/saveas?model=student.master&field=datas&id=%s&filename_field=result_report_file_name'%(self.id),
                'target': 'self',
                }


            
        
        
