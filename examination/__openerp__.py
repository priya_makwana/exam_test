{
    'name': 'Emipro Examination system',
    'summary': 'Design Examination System',
    'description': """
    
    """,
    'version': '1.0',
    'category': 'System',
    
    'depends': [
        'website','survey','hr'
    ],
    
    'data': [
        'template/asset.xml',
        'template/emipro_exam_page.xml',
        'template/exam.xml',
        'template/start_exam.xml',
        'template/design_question.xml',
        'template/finish_exam.xml',
        'report/student_question_answer_report.xml',
        'data/data.xml',
#         # Security 
        'security/interview_test_security.xml',
        'security/ir.model.access.csv',
         
        # View
        'view/survey_question.xml',
        'view/hr_view_ept.xml',
        'view/test_configuration.xml',
        'view/interview_test_view.xml'
    ],
    
    #'author'
    'author': 'Emipro',
    'website': 'http://www.emiprotechnologies.com/',
    
    # Technical
    'installable': True,
    'auto_install': False,
}
