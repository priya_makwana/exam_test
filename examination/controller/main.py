import json
from datetime import datetime
from openerp import SUPERUSER_ID
from openerp.addons.web import http
from openerp.addons.web.http import request
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT as DTF, ustr
import logging
_logger = logging.getLogger(__name__)
ET = 1800

class examination(http.Controller): 
    
    def _check_bad_cases(self, cr, uid, request, survey_obj, survey, user_input_obj, context=None):
        # In case of bad survey, redirect to surveys list
        if survey_obj.exists(cr, SUPERUSER_ID, survey.id, context=context) == []:
            return False

        # In case of auth required, block public user
        if survey.auth_required and uid == request.website.user_id.id:
            return request.website.render("survey.auth_required", {'survey': survey})

        # In case of non open surveys
        if survey.stage_id.closed:
            return request.website.render("survey.notopen")

        # If there is no pages
        if not survey.page_ids:
            return request.website.render("survey.nopages")

        # Everything seems to be ok
        return None
    
    def _check_bad_login(self,error_type):
        values = {
            'error':True,
            'error_type':error_type,
            }
        return values
    def _check_deadline(self, cr, uid, user_input, context=None):
        '''Prevent opening of the survey if the deadline has turned out

        ! This will NOT disallow access to users who have already partially filled the survey !'''
        if user_input.deadline:
            dt_deadline = datetime.strptime(user_input.deadline, DTF)
            dt_now = datetime.now()
            if dt_now > dt_deadline:  # survey is not open anymore
                return request.website.render("survey.notopen")
        return None
    
    def getstud(self,user_input):
        cr, uid, context = request.cr, request.uid, request.context
        stud_id = request.registry['student.master'].search(cr,SUPERUSER_ID,[('enrolment_no','=',user_input.token)])
        stud_data = request.registry['student.master'].browse(cr, uid,stud_id,context=context)
        return stud_data
    
    def _curr_time(self,last_time):
        ''' get current time and return the total min to find the
            remainig time '''
        now = datetime.now()
        end_time = "15:00:00"
        end_datetime = datetime.strptime(end_time, "%H:%M:%S")
        end_datetime = now.replace(hour=end_datetime.time().hour, minute=end_datetime.time().minute, second=end_datetime.time().second)
        
        # when the last time means get last recored time then exam resume with that time not with current time
        if last_time:
            now_temp = datetime.now()
            now = datetime.strptime(last_time,"%Y-%m-%d %H:%M:%S.%f");
            now= now_temp.replace(hour=now.time().hour, minute=now.time().minute, second=now.time().second)
        
        dis=end_datetime-now            
        return dis
        
    
    # temp controller to start exam new tab
    @http.route(['/exam_demo'],type='http', auth='public', website=True)
    def examination_info(self, **post):
        return request.render("examination.exam_start_page")
    
    
    @http.route(['/exam/start/'], type='http', auth='public', website=True)
    def start_exam_form(self, **post):
        cr, uid, context = request.cr, request.uid, request.context
        exam_domain_obj = request.registry['exam.domain']
        domain_ids = exam_domain_obj.search(cr, SUPERUSER_ID, [('active','=', True)], context=context)
        domain_data = exam_domain_obj.browse(cr, SUPERUSER_ID, domain_ids, context=context)
        values = {
            'record': domain_data,
            'status':True,
            }
        return request.render("examination.login_page",values);
       
    
    
    # Format the question with front end and store into the backend
    #design_question?model=survey.question&res_id=37&enable_editor=1
    @http.route('/design_question', type='http', auth="user", website=True)
    def formatQue(self, model, res_id,return_action,**kw):
        cr, uid, context = request.cr, request.uid, request.context
        res_id = int(res_id)
        obj_ids = request.registry[model].exists(request.cr, request.uid, [res_id], context=request.context)
        if not obj_ids:
            return request.redirect('/')
        record = request.registry[model].browse(cr, uid, res_id, context=context)
        values = {
            'record': record,
            'model': model,
            'res_id': res_id,
            'return_action': return_action
            }
        return request.website.render("examination.designer", values)

    @http.route('/format_design', type='http', auth="user", website=True)
    def formatQuestore(self,**post):
        cr, uid, context = request.cr, request.uid, request.context
        rc_id=post.get("oe_id",None)
        rc_code=post.get("oe_code",None)
        url=post.get("oe_url",None)
        if rc_id and rc_code:
            info = {
            'question': rc_code,
            }
            obj_ids=request.registry['survey.question'].write(cr, SUPERUSER_ID, [int(rc_id)],info, context=request.context)   
        return request.redirect(url) 

    # For Start Exam with login Form
    @http.route(['/examination'],type='http', auth='public', website=True)
    def examination_frame(self, **post):
        cr, uid, context = request.cr, request.uid, request.context
        survey_obj = request.registry['survey.survey']
        user_input_obj = request.registry['survey.user_input']
        student_master_obj = request.registry['student.master']
        enrollid=post.get('enrollid',None)
        domainid=post.get('domain',None)
        student_id = request.registry['student.master'].search(cr,SUPERUSER_ID,[('enrolment_no','=',enrollid)])
        if student_id:
            student_obj = student_master_obj.browse(cr, uid,student_id,context=context)
            valid_domain=student_obj.set_domain(domainid)
            if valid_domain == 'not properly set':
                value=self._check_bad_login('domain')
                return request.render("website.homepage",value);
            if student_obj.enrolment_no_confirm :
                value=self._check_bad_login('invalid_login')
                return request.render("website.homepage",value);
            survey_id = survey_obj.search(cr, SUPERUSER_ID, [('emipro_exam', '=', True)],context=context)[0]
            survey_data = survey_obj.browse(cr, SUPERUSER_ID, [survey_id], context=context)[0]
            user_input_count = user_input_obj.search_count(cr, SUPERUSER_ID, [('token', '=', enrollid)], context=context)
            if user_input_count == 0:
                vals = {'token': enrollid}
                vals['survey_id'] = survey_data.id
                user_input_rc = user_input_obj.create(cr, uid, vals, context=context)
            try:
                user_input_id = user_input_obj.search(cr, SUPERUSER_ID, [('token', '=', enrollid)], context=context)[0]
            except IndexError:  # Invalid token
                return request.website.render("website.403")
            user_input = user_input_obj.browse(cr, SUPERUSER_ID, [user_input_id], context=context)[0]
            values={"suvery_id":survey_data.id,"user_token":user_input.token}
            return request.render("examination.exam_main",values);
        else:  
            value=self._check_bad_login('enrollment')
            return request.render("website.homepage",value);
    
    
    @http.route(['/timer'], type='json', auth="public", website=True)
    def get_time(self,last_time=None,**post):
        remain_time=str(self._curr_time(last_time))
        #now_time=str(datetime.now())
        curr_time = datetime.now()
        #curr_time = str(curr_time.replace(hour=curr_time.time().hour, minute=curr_time.time().minute, second=curr_time.time().second))
        if last_time:
            now_temp = datetime.now()
            last_temp = datetime.strptime(last_time,"%Y-%m-%d %H:%M:%S.%f");
            last_temp= now_temp.replace(hour=last_temp.time().hour, minute=last_temp.time().minute, second=last_temp.time().second)
            temp=curr_time-last_temp
            #curr_time=curr_time-temp
            
            
        return {'time':remain_time,'curr':str(curr_time)};
    
    # json navagtion   
    @http.route(['/exam_nav'], type='json', auth="public", website=True)
    def navagtion(self, survey=None,token=None,**post):
        cr, uid, context = request.cr, request.uid, request.context
        ret = {}
        if survey and token:
            stud_obj = request.registry['student.master']
            questions_obj = request.registry['survey.question']
            student_domain_question_conf_obj = request.registry['student.exam.domain.configure']
#             qconf_obj = request.registry['student.test.configure']
            
            stud_ids = stud_obj.search(cr, uid, [('enrolment_no','=',token)],context=context)
            stud_data = stud_obj.browse(cr, uid,stud_ids,context=context)
            student_domain_question_conf_id = student_domain_question_conf_obj.search(cr,uid,[('student_id','=',stud_data.id),('domain_id','=',stud_data.domain_id.id)])
#             qconf_ids = qconf_obj.search(cr, uid, [('student_id','=',stud_data.id)],context=context)
            student_domain_question_conf_record = student_domain_question_conf_obj.browse(cr, uid,student_domain_question_conf_id,context=context)
            question_configure_ids = student_domain_question_conf_record.student_test_configure_ids
#             qconf_data = qconf_obj.browse(cr, uid,question_configure_ids,context=context)
            
            Pages=[]
            for qd in question_configure_ids:
                Pages.append(qd.page_ids.id)
            
            questions_ids = questions_obj.search(cr, uid, [('page_id', 'in',Pages)], context=context)
            question = questions_obj.search_count(cr, uid, [('page_id', 'in',Pages)], context=context)
            ret = {'question':question, 'page':len(Pages), 'pageids':Pages, 'questionids':questions_ids}
        return ret

    # json Get Ans_count / Question Navigation   
    @http.route(['/exam_ans_data/<model("survey.survey"):survey>/<string:token>'], type='json', auth="public", website=True)
    def get_ans(self, survey, token, **post):
        cr,context = request.cr, request.context
        user_input_obj = request.registry['survey.user_input']
        user_input_line_obj = request.registry['survey.user_input_line']
        ans_input_id = user_input_obj.search(cr,SUPERUSER_ID, [('survey_id', '=', survey.id), ('token', '=', token)], context=context)
        ans_input_line_id_count = user_input_line_obj.search_count(cr, SUPERUSER_ID, [('user_input_id', 'in', ans_input_id)], context=context)
        ans_input_line_ids = user_input_line_obj.search(cr, SUPERUSER_ID, [('user_input_id','in', ans_input_id)], context=context)
        ans_input_line_data = user_input_line_obj.browse(cr, SUPERUSER_ID, ans_input_line_ids, context=context)
        ans_list = []
        for answer_data in ans_input_line_data:
            ans_list.append(answer_data.page_id.id);
        
        ret_ans = {'ans_id':ans_input_id, 'count':ans_input_line_id_count, 'ans_input_ids':ans_input_line_ids, 'ans_input_line_data':ans_list}
        return ret_ans
      
    # Exam Question paper for navigation
    @http.route(['/exam_page/<model("survey.survey"):survey>/<string:token>/page/<string:pageno>'], type='http', auth="public", website=True)
    def exam_page(self, survey, token, pageno, **post):
        cr, uid, context = request.cr, request.uid, request.context
        survey_obj = request.registry['survey.survey']
        user_input_obj = request.registry['survey.user_input']
        try:
            user_input_id = user_input_obj.search(cr, SUPERUSER_ID, [('token', '=', token)])[0]
        except IndexError:  
            # Invalid token
            return request.website.render("website.404")
        else:
            user_input = user_input_obj.browse(cr, SUPERUSER_ID, [user_input_id], context=context)[0]
        page, page_nr, last = movepage(cr, uid, user_input, user_input.last_displayed_page_id.id, pageno, go_back=True, context=context)
        stud_rec = self.getstud(user_input)
        data = {'survey': survey, 'page': page, 'page_nr': page_nr, 'token': user_input.token,'student':stud_rec}
        if last:
            data.update({'last': True})
        return request.render("examination.question", data);
    
    
    # Exam Question paper for Next - prev   
    @http.route(['/exam_page/<model("survey.survey"):survey>/<string:token>', '/exam_page/<model("survey.survey"):survey>/<string:token>/<string:prev>'], type='http', auth="public", website=True)
    def fill_exam(self, survey, token, prev=None, **post):
        cr, uid, context = request.cr, request.uid, request.context
        survey_obj = request.registry['survey.survey']
        user_input_obj = request.registry['survey.user_input']

        # Controls if the survey can be displayed
        errpage = self._check_bad_cases(cr, uid, request, survey_obj, survey, user_input_obj, context=context)
        if errpage:
            return errpage

        # Load the user_input
        try:    
            user_input_id = user_input_obj.search(cr, SUPERUSER_ID, [('token', '=', token)])[0]
        except IndexError:  # Invalid token
            return request.website.render("website.404")
        else:
            user_input = user_input_obj.browse(cr, SUPERUSER_ID, [user_input_id], context=context)[0]

        errpage = self._check_deadline(cr, uid, user_input, context=context)
        if errpage:
            return errpage
        
        stud_rec = self.getstud(user_input)
        
        # Select the right page
        if user_input.state == 'new':  # First page
            page, page_nr, last = move_page(cr, uid, user_input, 0, go_back=False, context=context)
            data = {'survey': survey, 'page': page, 'page_nr': page_nr, 'token': user_input.token,'student':stud_rec}
            if last:
                data.update({'last': True})
            return request.website.render("examination.question", data);
        
        elif user_input.state == 'done':  # Display success message
            return request.website.render('survey.sfinished', {'survey': survey,
                                                               'token': token,
                                                               'user_input': user_input,'student':stud_rec})
        elif user_input.state == 'skip':
            flag = (True if prev and prev == 'prev' else False)
            page, page_nr, last = move_page(cr, uid, user_input, user_input.last_displayed_page_id.id, go_back=flag, context=context)

            # special case if you click "previous" from the last page, then leave the survey, then reopen it from the URL, avoid crash
            if not page:
                page, page_nr, last = move_page(cr, uid, user_input, user_input.last_displayed_page_id.id, go_back=True, context=context)

            data = {'survey': survey, 'page': page, 'page_nr': page_nr, 'token': user_input.token,'student':stud_rec}
            if last:
                data.update({'last': True})
            return request.render("examination.question", data);
        else:
            return request.website.render("website.403") 
    
    
    # For Prefill the Data 
    @http.route(['/que/prefill/<model("survey.survey"):survey>/<string:token>',
                 '/que/prefill/<model("survey.survey"):survey>/<string:token>/<model("survey.page"):page>'],
                type='http', auth='public', website=True)
    def prefill(self, survey, token, page=None, **post):
        cr, uid, context = request.cr, request.uid, request.context
        user_input_line_obj = request.registry['survey.user_input_line']
        ret = {}

        # Fetch previous answers
        if page:
            ids = user_input_line_obj.search(cr, SUPERUSER_ID, [('user_input_id.token', '=', token), ('page_id', '=', page.id)], context=context)
        else:
            ids = user_input_line_obj.search(cr, SUPERUSER_ID, [('user_input_id.token', '=', token)], context=context)
        previous_answers = user_input_line_obj.browse(cr, uid, ids, context=context)

        # Return non empty answers in a JSON compatible format
        for answer in previous_answers:
            if not answer.skipped:
                answer_tag = '%s_%s_%s' % (answer.survey_id.id, answer.page_id.id, answer.question_id.id)
                answer_value = None
                if answer.answer_type == 'free_text':
                    answer_value = answer.value_free_text
                elif answer.answer_type == 'text' and answer.question_id.type == 'textbox':
                    answer_value = answer.value_text
                elif answer.answer_type == 'text' and answer.question_id.type != 'textbox':
                # here come comment answers for matrices, simple choice and multiple choice
                    answer_tag = "%s_%s" % (answer_tag, 'comment')
                    answer_value = answer.value_text
                elif answer.answer_type == 'number':
                    answer_value = answer.value_number.__str__()
                elif answer.answer_type == 'date':
                    answer_value = answer.value_date
                elif answer.answer_type == 'suggestion' and not answer.value_suggested_row:
                    answer_value = answer.value_suggested.id
                elif answer.answer_type == 'suggestion' and answer.value_suggested_row:
                    answer_tag = "%s_%s" % (answer_tag, answer.value_suggested_row.id)
                    answer_value = answer.value_suggested.id
                if answer_value:
                    dict_soft_update(ret, answer_tag, answer_value)
                else:
                    _logger.warning("[survey] No answer has been found for question %s marked as non skipped" % answer_tag)
        return json.dumps(ret)
    
    
    # Submit the question  - ans     
    @http.route(['/que/submit/<model("survey.survey"):survey>'],
                type='http', methods=['POST'], auth='public', website=True)
    def submit(self, survey, **post):
        _logger.debug('Incoming data: %s', post)
        page_id = int(post.get('page_id', "0"))
        page_no = int(post.get('page_nav_id', "0"))
        cr, uid, context = request.cr, request.uid, request.context
        survey_obj = request.registry['survey.survey']
        questions_obj = request.registry['survey.question']
        questions_ids = questions_obj.search(cr, uid, [('page_id', '=', page_id)], context=context)
        questions = questions_obj.browse(cr, uid, questions_ids, context=context)

        # Answer validation
        errors = {}
        for question in questions:  
            answer_tag = "%s_%s_%s" % (survey.id, page_id, question.id)
            errors.update(questions_obj.validate_question(cr, uid, question, post, answer_tag, context=context))
        
        ret = {}
        if (len(errors) != 0):
            # Return errors messages to webpage
            ret['errors'] = errors
        else:
            # Store answers into database
            user_input_obj = request.registry['survey.user_input']
            user_input_line_obj = request.registry['survey.user_input_line']
            try:
                user_input_id = user_input_obj.search(cr, SUPERUSER_ID, [('token', '=', post['token'])], context=context)[0]
            except KeyError:  # Invalid token
                return request.website.render("website.403")
            
            user_input = user_input_obj.browse(cr, SUPERUSER_ID, user_input_id, context=context)
            user_id = uid if user_input.type != 'link' else SUPERUSER_ID
            
            for question in questions:
                answer_tag = "%s_%s_%s" % (survey.id, page_id, question.id)
                user_input_line_obj.save_lines(cr, user_id, user_input_id, question, post, answer_tag, context=context)
              
            # Varify the navigation
            vals = {'last_displayed_page_id': page_id}    
            next_page, _, last = move_page(cr, uid, user_input, page_id, go_back=True, context=context)
            if next_page is None:
                    vals.update({'state': 'done'})
            else:
                    vals.update({'state': 'skip'})
            
            
            # Move as click on button / navigation 
            btnsubmit = post.get('button_submit', 'custome')
            if btnsubmit == "previous" :            
                ret['redirect'] = '/exam_page/%s/%s/prev' % (survey.id, post['token'])
            if btnsubmit == "custome" :
                ret['redirect'] = '/exam_page/%s/%s/page/%s' % (survey.id, post['token'], page_no)
            if btnsubmit == "next":
                ret['redirect'] = '/exam_page/%s/%s' % (survey.id, post['token'])
            if btnsubmit == "finish":
                vals.update({'state': 'done'})
                ret['redirect'] = '/exam_page/%s/%s' % (survey.id, post['token'])
            user_input_obj.write(cr, user_id, user_input_id, vals, context=context)
            if user_input.state == 'done':
                score = user_input.calculate_result()
        return json.dumps(ret)    
    
    # Printing routes
    @http.route(['/survey/print/<model("survey.survey"):survey>',
                 '/survey/print/<model("survey.survey"):survey>/<string:token>'],
                type='http', auth='public', website=True)
    def print_survey(self, survey, token=None, **post):
        '''Display an survey in printable view; if <token> is set, it will
        grab the answers of the user_input_id that has <token>.'''
        cr, uid, context = request.cr, request.uid, request.context
        survey_user_input_obj = request.registry['survey.user_input']
        survey_user_input_ids = survey_user_input_obj.search(cr, uid, [('token', '=', token)], context=context)
        user_input = survey_user_input_obj.browse(cr, uid, survey_user_input_ids, context=context)
        
        return request.website.render('survey.survey_print',
                                      {'survey': survey,
                                       'user_input':user_input,
                                       'token': token,
                                       'page_nr': 0,
                                       'quizz_correction': True if survey.quizz_mode and token else False})

        
        
def dict_soft_update(dictionary, key, value):
    if key in dictionary:
        dictionary[key].append(value)
    else:
        dictionary.update({key: [value]})

def movepage(cr, uid, user_input, page_id, moveid, go_back=False, context=None):
    survey = user_input.survey_id
    stud_obj = request.registry['student.master']
    stud_ids = stud_obj.search(cr, uid, [('enrolment_no','=',user_input.token)],context=context)
    stud_data = stud_obj.browse(cr, uid,stud_ids,context=context)
    student_domain_question_conf_obj = request.registry['student.exam.domain.configure']
#     qconf_obj = request.registry['student.test.configure']
#     qconf_ids = qconf_obj.search(cr, uid, [('student_id','=',stud_data.id)],context=context)
#     qconf_data = qconf_obj.browse(cr, uid,qconf_ids,context=context)
    student_domain_question_conf_id = student_domain_question_conf_obj.search(cr,uid,[('student_id','=',stud_data.id),('domain_id','=',stud_data.domain_id.id)])
    student_domain_question_conf_record = student_domain_question_conf_obj.browse(cr, uid,student_domain_question_conf_id,context=context)
    question_configure_ids = student_domain_question_conf_record.student_test_configure_ids
    Page=[]
    for qd in question_configure_ids:
        Page.append(qd.page_ids)
        
    pages = list(enumerate(Page))
    current_page_index = pages.index((filter(lambda p: p[1].id == page_id, pages))[0])
    
    mid = int(moveid)
    # First Page
    if (mid - 1) == 0:
        return (pages[mid - 1][1], 0, len(pages) == 1)
    # Last Page
    if (mid - 1) == len(pages) - 1:
        return (pages[mid - 1][1], (mid - 1), True)
    # This will show a regular page
    else:
        return (pages[mid - 1][1], current_page_index + 1, False)
        


def move_page(cr, uid, user_input, page_id, go_back=False, context=None):
        survey = user_input.survey_id
        stud_obj = request.registry['student.master']
        stud_ids = stud_obj.search(cr, uid, [('enrolment_no','=',user_input.token)],context=context)
        stud_data = stud_obj.browse(cr, uid,stud_ids,context=context)
        student_domain_question_conf_obj = request.registry['student.exam.domain.configure']
#         qconf_obj = request.registry['student.test.configure']
#         qconf_ids = qconf_obj.search(cr, uid, [('student_id','=',stud_data.id)],context=context)
#         qconf_data = qconf_obj.browse(cr, uid,qconf_ids,context=context)
        student_domain_question_conf_obj = request.registry['student.exam.domain.configure']
        student_domain_question_conf_id = student_domain_question_conf_obj.search(cr,uid,[('student_id','=',stud_data.id),('domain_id','=',stud_data.domain_id.id)])
        student_domain_question_conf_record = student_domain_question_conf_obj.browse(cr, uid,student_domain_question_conf_id,context=context)
        question_configure_ids = student_domain_question_conf_record.student_test_configure_ids
        Page=[]
        for qd in question_configure_ids:
            Page.append(qd.page_ids)
#         if not question_configure_ids:
#             college_test_obj = request.registry['college.test.configuration'].search([('college_id','=',stud_data.college_id.id),('domain_id','=',stud_data.domain_id.id)],limit=1)
#             for qd in college_test_obj.page_ids:
#                 Page.append(qd.page_ids)
                
        pages = list(enumerate(Page))
        
        # First page
        if page_id == 0:
            return (pages[0][1], 0, len(pages) == 1)
        
        
        current_page_index = pages.index((filter(lambda p: p[1].id == page_id, pages))[0])

        # All the pages have been displayed
        if current_page_index == len(pages) - 1 and not go_back:
            return (None, -1, False)
        
        # Let's get back to prev page
        elif go_back and survey.users_can_go_back:
            return (pages[current_page_index - 1][1], current_page_index - 1, False)
        else:
            # This will show the last page
            if current_page_index == len(pages) - 2:
                return (pages[current_page_index + 1][1], current_page_index + 1, True)
            # This will show a regular page
            else:#if pages[current_page_index +1] and pages[current_page_index + 1][0] and pages[current_page_index + 1][1]:
                return (pages[current_page_index + 1][1], current_page_index + 1, False)


